
.. index::
   pair: Annotation ; annotator
   ! annotator

.. _annotator:

====================================================================
**annotator** (Image annotation for Elementary OS and **Debian**)
====================================================================

- https://github.com/phase1geo/Annotator

Overview
============

Annotate your images and let a picture say 1000 words.

- Load image from the file system or clipboard.
- Add shapes, stickers, text, drawings, and other callouts to highlight image details.
- Add magnifiers to enhance image details.
- Blur out portions of the image to obfuscate data.
- Crop, resize and add image borders.
- Control colors, line thickness and font details.
- Zoom support.
- Unlimited undo/redo of any change.
- Export to JPEG, PNG, TIFF, BMP, PDF and SVG image formats.
- Support for copying annotated image to clipboard.
- Printer support.

Description 2
================

Not just limited to adding enhancements to an image, but you also get
some other functionalities with Annotator.
Some of the key features are:

- Ability to add a text
- Customize the text color, adjust the size, tweak the highlight color, and more
- Use magnifying tool to focus on a particular point in image
- Tweak the magnifier to change the angle, and size
- Add visual icons to express something
- Use pencil to write, useful for trackpad users
- Blur tool to obfuscate important or unnecessary details
- Counter icons to highlight important any sort of order in an image
- Supports multiple fonts
- Adjust the border width and lines for shapes and texts
- Various shapes like arrow, circle, and star available
- Ability to resize and crop images


Installing Annotator in Linux
================================

Primarily, Annotator is available on AppCenter for elementaryOS.

But, fortunately, it is a Flatpak package, so that you can install it
on any Linux distribution.

All you need to do is head to the AppCenter store’s link and then
download/open the Flatpak ref file using the software installer.

We have more information on this in our Flatpak guide, if you need help.


To build from the source or explore more about it, you can check out its
GitHub page.

Installation
=============

Debian
--------

You will need the following dependencies to build Annotator::

    meson
    valac
    debhelper
    gobject-2.0
    glib-2.0
    libgee-0.8-dev
    libgranite-dev
    libxml2-dev
    libgtk-3-dev
    libhandy-1-dev

To install Annotator from source, run::

    ./app install

::

    ✦ ❯ sudo ./app install
    The Meson build system
    Version: 0.56.2
    Source dir: /opt/Annotator
    Build dir: /opt/Annotator/build
    Build type: native build
    Project name: com.github.phase1geo.annotator
    Project version: 1.0.0
    C compiler for the host machine: cc (gcc 10.2.1 "cc (Debian 10.2.1-6) 10.2.1 20210110")
    C linker for the host machine: cc ld.bfd 2.35.2
    Vala compiler for the host machine: valac (valac 0.48.17)
    Host machine cpu family: x86_64
    Host machine cpu: x86_64
    Found pkg-config: /usr/bin/pkg-config (0.29.2)
    Program glib-compile-resources found: YES (/usr/bin/glib-compile-resources)
    Configuring config.h using configuration
    Found pkg-config: /usr/bin/pkg-config (0.29.2)
    Run-time dependency gtk+-3.0 found: YES 3.24.24
    Library m found: YES
    Run-time dependency gobject-2.0 found: YES 2.66.8
    Run-time dependency glib-2.0 found: YES 2.66.8
    Run-time dependency gee-0.8 found: YES 0.20.4
    Run-time dependency granite found: YES 5.5.0
    Dependency gtk+-3.0 found: YES 3.24.24 (cached)
    Run-time dependency libxml-2.0 found: YES 2.9.10
    Run-time dependency libhandy-1 found: YES 1.0.3
    Program meson/post_install.py found: YES (/opt/Annotator/meson/post_install.py)
    Build targets in project: 11

    Found ninja-1.10.1 at /usr/bin/ninja
    [1/65] Generating annotator-resources_h with a custom command
    Un prétraitement xml-stripblanks a été demandé, mais XMLLINT n’est pas défini et xmllint n’est pas dans le chemin PATH
    [2/65] Generating annotator-resources_c with a custom command
    Un prétraitement xml-stripblanks a été demandé, mais XMLLINT n’est pas défini et xmllint n’est pas dans le chemin PATH
    [65/65] Linking target com.github.phase1geo.annotator
    [0/1] Installing files.
    Installing data/com.github.phase1geo.annotator.desktop to /usr/share/applications
    Installing data/com.github.phase1geo.annotator.appdata.xml to /usr/share/metainfo
    Installing com.github.phase1geo.annotator to /usr/bin
    Installing /opt/Annotator/data/images/icons/16/com.github.phase1geo.annotator.svg to /usr/share/icons/hicolor/16x16/apps
    Installing /opt/Annotator/data/images/icons/16/com.github.phase1geo.annotator.svg to /usr/share/icons/hicolor/16x16@2/apps
    Installing /opt/Annotator/data/images/icons/24/com.github.phase1geo.annotator.svg to /usr/share/icons/hicolor/24x24/apps
    Installing /opt/Annotator/data/images/icons/24/com.github.phase1geo.annotator.svg to /usr/share/icons/hicolor/24x24@2/apps
    Installing /opt/Annotator/data/images/icons/32/com.github.phase1geo.annotator.svg to /usr/share/icons/hicolor/32x32/apps
    Installing /opt/Annotator/data/images/icons/32/com.github.phase1geo.annotator.svg to /usr/share/icons/hicolor/32x32@2/apps
    Installing /opt/Annotator/data/images/icons/48/com.github.phase1geo.annotator.svg to /usr/share/icons/hicolor/48x48/apps
    Installing /opt/Annotator/data/images/icons/48/com.github.phase1geo.annotator.svg to /usr/share/icons/hicolor/48x48@2/apps
    Installing /opt/Annotator/data/images/icons/64/com.github.phase1geo.annotator.svg to /usr/share/icons/hicolor/64x64/apps
    Installing /opt/Annotator/data/images/icons/64/com.github.phase1geo.annotator.svg to /usr/share/icons/hicolor/64x64@2/apps
    Installing /opt/Annotator/data/images/icons/128/com.github.phase1geo.annotator.svg to /usr/share/icons/hicolor/128x128/apps
    Installing /opt/Annotator/data/images/icons/128/com.github.phase1geo.annotator.svg to /usr/share/icons/hicolor/128x128@2/apps
    Installing /opt/Annotator/data/com.github.phase1geo.annotator.gschema.xml to /usr/share/glib-2.0/schemas
    Running custom install script '/usr/bin/meson --internal gettext install --subdir=po --localedir=share/locale --pkgname=com.github.phase1geo.annotator'
    Installing /opt/Annotator/build/po/es.gmo to /usr/share/locale/es/LC_MESSAGES/com.github.phase1geo.annotator.mo
    Installing /opt/Annotator/build/po/it.gmo to /usr/share/locale/it/LC_MESSAGES/com.github.phase1geo.annotator.mo
    Installing /opt/Annotator/build/po/nl.gmo to /usr/share/locale/nl/LC_MESSAGES/com.github.phase1geo.annotator.mo
    Installing /opt/Annotator/build/po/oc.gmo to /usr/share/locale/oc/LC_MESSAGES/com.github.phase1geo.annotator.mo
    Installing /opt/Annotator/build/po/pt_BR.gmo to /usr/share/locale/pt_BR/LC_MESSAGES/com.github.phase1geo.annotator.mo
    Installing /opt/Annotator/build/po/sk.gmo to /usr/share/locale/sk/LC_MESSAGES/com.github.phase1geo.annotator.mo
    Running custom install script '/opt/Annotator/meson/post_install.py'
    Compiling gsettings schemas…
    Updating icon cache…
    Compiling mime types…


tree -L 3
----------

::

    ✦ ❯ tree -L 3
    .
    ├── app
    ├── AUTHORS
    ├── build
    │   ├── annotator-resources.c
    │   ├── annotator-resources.h
    │   ├── build.ninja
    │   ├── com.github.phase1geo.annotator
    │   ├── com.github.phase1geo.annotator.p


Run
-----

To run Annotator, run::

    build/com.github.phase1geo.annotator


::

    /opt/Annotator/build/com.github.phase1geo.annotator

Alias
------

::

    alias annotate='/opt/Annotator/build/com.github.phase1geo.annotator'


Thoughts on Using Annotator
===============================

- https://itsfoss.com/take-screenshot-linux/

I find it useful for my work, considering we need to deal with various
screenshots every day. And, the more descriptive, the better the images
for our readers to understand.

Usually, I use Flameshot as my screenshot tool and add annotations
available through it.
There are other `screenshot tools <https://itsfoss.com/take-screenshot-linux/>`_ available for Linux, but I prefer
Flameshot for its quick annotation features.

However, you cannot use Flameshot to add annotations to existing images.

Hence, Annotator came in handy to edit existing screenshots or any other
images needed.

All of its features worked well in my brief usage, but I’d want to edit
and re-size the text to be more intuitive. Other than that, I have no complaints.

I’d recommend you give this a try if you need to add highlights/annotations
to the images on your system. Let me know your thoughts in the comments down below.
