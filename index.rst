
.. raw:: html

   <a rel="me" href="https://framapiaf.org/@pvergain"></a>
   <a rel="me" href="https://babka.social/@pvergain"></a>


|FluxWeb| `RSS <https://gdevops.frama.io/documentation/tools/rss.xml>`_

.. _doc_tools:

==========================
**Documentation tools**
==========================

- http://blog.smartbear.com/software-quality/bid/256072/13-reasons-your-open-source-docs-make-people-want-to-scream
- http://brikis98.blogspot.de/2014/05/you-are-what-you-document.html
- http://redaction-technique.org/index.html

.. toctree::
   :maxdepth: 3

   basic-tools/basic-tools
   annotator/annotator
   autodocstring/autodocstring
   dash/dash
   diag/diag
   diagrams/diagrams
   drawio/drawio
   flameshot/flameshot
   graphviz/graphviz
   jinja/jinja
   pygments/pygments
   rst_to_myst/rst_to_myst
   scribe/scribe.rst
   zeal/zeal
   wiki/wiki
