
.. index::
   pair: scribe-cemea ; transcripteur libre Audio/Vidéo en Texte
   pair: scribe-dema ; transcription
   ! scribe-cemea

.. _scribe_cemea:

===============================================================
**scribe-cemea** un transcripteur libre Audio/Vidéo en Texte
===============================================================

- https://gitlab.cemea.org/mallette/scribe
- https://scribe.cemea.org


Description
==============

Scribe est un service de transcription gratuit, dont le code est publié sous
licence "libre". Vous pouvez donc disposer de votre propre Scribe.

Les `Ceméa, qui soutiennent les Communs Numériques <https://cemea.asso.fr/les-champs-d-action/medias-et-numerique-libre>`_, peuvent vous accompagner
dans l'hébergement de Scribe sur vos serveurs.
