.. index::
   pair: Jinja ; Template
   ! Jinja

.. _tool_jinja:

============================
**jinja**
============================

- https://jinja.palletsprojects.com/en/3.1.x/
- https://github.com/pallets/jinja
- https://x.com/PalletsTeam
- https://x.com/davidism
- https://github.com/abey79/vsketch/tree/master/docs/_templates/autoapi

Description
============

Jinja is a fast, expressive, extensible templating engine.
Special placeholders in the template allow writing code similar to Python
syntax. Then the template is passed data to render the final document.

It includes:

- Template inheritance and inclusion.
- Define and import `macros <macros_vsketch>` within templates.
- HTML templates can use autoescaping to prevent XSS from untrusted user input.
- A sandboxed environment can safely render untrusted templates.
- AsyncIO support for generating templates and calling async functions.
- I18N support with Babel.
- Templates are compiled to optimized Python code just-in-time and cached,
  or can be compiled ahead-of-time.
- Exceptions point to the correct line in templates to make debugging easier.
- Extensible filters, tests, functions, and even syntax.-

Jinja's philosophy is that while application logic belongs in Python if
possible, it shouldn't make the template designer's job difficult by
restricting functionality too much.


Versions
===========

.. toctree::
   :maxdepth: 3

   versions/versions
