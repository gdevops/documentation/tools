
.. index::
   pair: diagrams ; Python
   ! diagrams

.. _diagrams_python:

==========================================================================
**Diagrams** (lets you draw the cloud system architecture in Python code)
==========================================================================

- https://github.com/mingrammer/diagrams
- https://diagrams.mingrammer.com/
- https://diagrams.mingrammer.com/docs/getting-started/examples


Annonce de Robert Stéphane
============================

- https://blog.stephane-robert.info/post/devops-diagram-as-code/
- :ref:`diagrams_ref`


.. _diagrams_example:

advanced-web-service-with-on-premise-with-colors-and-labels
==============================================================

- https://diagrams.mingrammer.com/docs/getting-started/examples#advanced-web-service-with-on-premise-with-colors-and-labels

.. code-block:: python
   :linenos:

    from diagrams import Cluster, Diagram
    from diagrams.onprem.analytics import Spark
    from diagrams.onprem.compute import Server
    from diagrams.onprem.database import PostgreSQL
    from diagrams.onprem.inmemory import Redis
    from diagrams.onprem.aggregator import Fluentd
    from diagrams.onprem.monitoring import Grafana, Prometheus
    from diagrams.onprem.network import Nginx
    from diagrams.onprem.queue import Kafka

    with Diagram("Advanced Web Service with On-Premise", show=False):
        ingress = Nginx("ingress")

        metrics = Prometheus("metric")
        metrics << Grafana("monitoring")

        with Cluster("Service Cluster"):
            grpcsvc = [
                Server("grpc1"),
                Server("grpc2"),
                Server("grpc3")]

        with Cluster("Sessions HA"):
            primary = Redis("session")
            primary - Redis("replica") << metrics
            grpcsvc >> primary

        with Cluster("Database HA"):
            primary = PostgreSQL("users")
            primary - PostgreSQL("replica") << metrics
            grpcsvc >> primary

        aggregator = Fluentd("logging")
        aggregator >> Kafka("stream") >> Spark("analytics")

        ingress >> grpcsvc >> aggregator

.. figure:: images/exemple_1.png
   :align: center

   https://diagrams.mingrammer.com/docs/getting-started/examples#advanced-web-service-with-on-premise-with-colors-and-labels
