
.. index::
   pair: diagrams ; drawio
   ! drawio

.. _drawio:

=======================================
**drawio** (diagrams.net)
=======================================

- https://www.diagrams.net/
- https://github.com/jgraph/drawio-desktop/releases/tag/v18.0.1
