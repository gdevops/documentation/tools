
.. index::
   pair: Annotation ; flameshot
   ! flameshot

.. _flameshot:

===============================================================
**flameshot** (Powerful yet simple to use screenshot software)
===============================================================

- https://github.com/lupoDharkael/flameshot

.. figure:: Flameshot.svg
   :align: center
