.. index::
   pair:  basic tool ; resize-images
   ! resize-images

.. _resize-images:

================================================
**resize-images: basic tool to resize images** 
================================================


- https://framagit.org/gdevops/util/images


resize_images_800_webp.bash
=============================

- https://framagit.org/gdevops/util/images/-/blob/main/resize_images_800_webp.bash?ref_type=heads

.. code-block:: bash

    #!/usr/bin/env bash
    # The image size_max will be 800

    size_max=800
    mkdir ${size_max}

    for filename in *.jpg; do
        input=${filename}
        name=${filename%.*}
        # suffixe=${filename##*.}
        suffixe='webp'
        output="800/"${name}_${size_max}.${suffixe}
        echo "Input: ${input}"
        echo "File Name: ${name}"
        echo "File Extension: ${suffixe}"
        echo "output: ${output}"

        convert ${input} -resize ${size_max} ${output}
    done



How to use it ?
====================

- 1) put your files on an external drive
- 2) put this script on the same place and launch this script against jpg files
- 3) the resized images will be produced under the '800' directory  

Resulting file sizes
----------------------------

.. literalinclude:: ../images/tree.txt

.. figure:: ../images/files.webp
