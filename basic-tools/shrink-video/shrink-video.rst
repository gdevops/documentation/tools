.. index::
   pair:  basic tool ; shrink a video
   pair: shrink; video

.. _shrink_videos:

=======================
**shrink a video**
=======================


- https://framagit.org/gdevops/util/videos


shrink_videos_hd480.bash
=============================

- https://framagit.org/gdevops/util/videos/-/blob/main/shrink_videos_hd480.bash?ref_type=heads

.. code-block:: bash

    #!/usr/bin/env bash
    #
    # The HD definition will be 'hd480'
    #
    # https://shrink-my-video.onrender.com/shrink-my-video/
    # https://gdevops.gitlab.io/tuto_lowtech/
    #
    # https://shrink-my-video.onrender.com/shrink-my-video/
    hd_definition=hd480
    mkdir ${hd_definition}

    for filename in *.mp4; do
        input=${filename}
        name=${filename%.*}
        suffixe=${filename##*.}
        output="hd480/"${name}_${hd_definition}.${suffixe}
        echo "Input: ${input}"
        echo "File Name: ${name}"
        echo "File Extension: ${suffixe}"
        echo "output: ${output}"

        ffmpeg -i ${input} -c:v libx264 -s ${hd_definition} -crf 22 -c:a aac -b:a 160k -vf "scale=iw*sar:ih,setsar=1" ${output}

    done


How to use it ?
====================

- 1) put your files on an external drive
- 2) put this script on the same place and launch this script against a mp4 files
- 3) the shrink videos will be produced under the hd480 directory  


Resulting file sizes
----------------------------

.. literalinclude:: ../images/tree.txt

.. figure:: ../images/files.webp
