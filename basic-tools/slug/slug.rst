.. index::
   pair:  basic tool ; slug
   ! slugify
   ! slug
   ! identifiant pour adresse URL

.. _slug:

============================================================
**slug : to give an url (identifiant pour adresse URL)** 
============================================================

- https://fr.wikipedia.org/wiki/Slug_(journalisme)
- https://vitrinelinguistique.oqlf.gouv.qc.ca/fiche-gdt/fiche/26532630/identifiant-pour-adresse-url

The python slug.py module
=============================

- https://framagit.org/gdevops/util/slug/-/blob/main/slug.py?ref_type=heads

.. literalinclude:: slug.py
   :language: python


What does mean the term slugify ?
=====================================

**The term "slugify" refers to the process of taking a piece of text and
removing any characters that are not letters or numbers, and then
replacing any spaces with dashes or underscores**.

This is often used when creating URLs or file names from a piece of text,
in order to make the resulting string more user-friendly and easier to
read.

For example, the text "Hello World!" could be slugified to "hello-world"
or "hello_world".


How does it work ?
======================

::

    poetry shell
    
::

    python slug.py --text "Rassemblement contre l’antisémitisme et les violences sexistes et sexuelles"


The slug (identifiant d'URL)::

    rassemblement-contre-lantisemitisme-et-les-violences-sexistes-et-sexuelles
   
