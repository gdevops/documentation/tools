.. index::
   ! Basic documentation tools

.. _basic_doc_tools:

==================================
**Basic documentation tools**
==================================

.. toctree::
   :maxdepth: 3

   format-texte/format-texte   
   resize-images/resize-images   
   slug/slug
   shrink-video/shrink-video   
