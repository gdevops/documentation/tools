.. index::
   pair:  basic tool ; Format  texte
   pair: Format  ; Texte
   ! Format texte

.. _format_texte:

===============================================
**Format text: the base tool** 
===============================================

- https://framagit.org/gdevops/util/texte

formate.bash
=====================

- https://framagit.org/gdevops/util/texte/-/blob/main/formate.bash?ref_type=heads

.. literalinclude:: formate.bash


How does it work ?
======================


- 1) put your text in the t.txt file 
- 2) put this script on the same place and launch the script
- 3) get the result in the output.rst file
