.. index::
   pair: pygments ; pull requests

.. _pygments_pull_requests:

=======================
pygments pull requests
=======================

.. seealso::

   - https://github.com/pygments/pygments/pulls
