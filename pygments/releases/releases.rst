.. index::
   pair: pygments ; releases

.. _pygments_releases:

=======================
pygments releases
=======================

- https://pygments.org/docs/changelog/
- https://github.com/pygments/pygments/graphs/contributors
- https://pygments.org/docs/authors/
- https://pypi.org/project/Pygments/#history
- https://github.com/pygments/pygments/blob/master/CHANGES

.. toctree::
   :maxdepth: 3

   2.10.0/2.10.0
   2.9.0/2.9.0
   2.8.0/2.8.0
   2.7.0/2.7.0
   2.6.1/2.6.1
   2.6.0/2.6.0
   2.5.2/2.5.2
   2.5.0/2.5.0
   2.4.0/2.4.0
   2.3.1/2.3.1
