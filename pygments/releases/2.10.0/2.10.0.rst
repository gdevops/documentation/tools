.. index::
   pair: pygments ; 2.10.0 (2021-08-15)

.. _pygments_2_10_0:

=================================
pygments 2.10.0 (2021-08-15)
=================================


- https://github.com/pygments/pygments/releases/tag/2.10.0
- https://pygments.org/docs/changelog/#version-2-10-0


Version 2.10.0
================


Added
------

- ASC armored files (#1807)
- GSQL (#1809, #1866)
- Javascript REPL (#1825)
- procfile (#1808)
- Smithy (#1878, #1879)
