.. index::
   pair: pygments ; Documentation
   ! pygments

.. _pygments:

===========================================
**pygments** (generic syntax highlighter)
===========================================

.. seealso::

   - http://pygments.org/
   - https://x.com/NIV_Anteru
   - https://github.com/pygments/pygments
   - https://github.com/pygments/pygments/graphs/contributors
   - https://pygments.org/languages/


.. figure:: logo_pygments.png
   :align: center

.. toctree::
   :maxdepth: 3

   definition/definition
   faq/faq
   formatters/formatters
   lexers/lexers
   styles/styles
   issues/issues
   pull_requests/pull_requests
   releases/releases
