.. index::
   pair: autoDocstring ; IDE
   ! autoDocstring

.. _vscodium_autoDocstring:

============================
VSCodium/**autodocstring**
============================

- https://github.com/NilsJPWerner/autoDocstring

Description
============

**autodocstring** is a VSCodium extension that generates docstrings for
python files.

.. figure:: settings/extension.png
   :align: center


Settings
========

.. figure:: settings/format_type.png
   :align: center


VSCodium
===========


- :ref:`vscodium`
