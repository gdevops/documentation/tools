

.. index::
   pair: blockdiag ; Documentation
   ! blockdiag

.. _blockdiag:

=======================================
blockdiag
=======================================

.. seealso::

   - http://blockdiag.com/en/blockdiag/index.html
   - https://github.com/blockdiag/blockdiag
   - https://github.com/blockdiag/blockdiag.com

.. toctree::
   :maxdepth: 3

   installation/installation
   sphinxcontrib/sphinxcontrib
