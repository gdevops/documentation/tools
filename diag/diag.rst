
.. index::
   pair: diagrams ; Documentation
   ! diagrams

.. _diagrams:

=======================================
diagrams
=======================================

- http://blockdiag.com/en/index.html
- https://github.com/blockdiag
- http://interactive.blockdiag.com/


.. figure:: logo_diagrams.png
   :align: center


.. toctree::
   :maxdepth: 3

   blockdiag/blockdiag
   seqdiag/seqdiag
   actdiag/actdiag
   nwdiag/nwdiag
   interactive_shell/interactive_shell
