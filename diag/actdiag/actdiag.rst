

.. index::
   pair: actdiag ; Documentation
   ! actdiag

.. _actdiag:

=======================================
actdiag
=======================================

.. seealso::

   - http://blockdiag.com/en/actdiag/index.html

.. toctree::
   :maxdepth: 3

   installation/installation
